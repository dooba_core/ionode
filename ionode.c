/* Dooba SDK
 * ioNode Platform
 */

// External Includes
#include <dio/dio.h>

// Internal Includes
#include "ionode.h"

// Initialize Board
void ionode_init()
{
	// Configure Activity LED Pin
	dio_output(IONODE_ACT_LED);
	dio_lo(IONODE_ACT_LED);
}

// Flash Activity LED
void ionode_act_led(uint8_t s)
{
	// Flash it
	if(s)						{ dio_hi(IONODE_ACT_LED); }
	else						{ dio_lo(IONODE_ACT_LED); }
}
