/* Dooba SDK
 * ioNode Platform
 */

#ifndef	__IONODE_H
#define	__IONODE_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Activity LED
#define	IONODE_ACT_LED						12

// Initialize Board
extern void ionode_init();

// Flash Activity LED
extern void ionode_act_led(uint8_t s);

#endif
